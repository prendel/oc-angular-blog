import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  posts = [
    {
      title: 'First Post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non metus id ipsum rutrum laoreet. Nulla in sagittis mauris. Nam eget mollis urna. Aliquam vitae neque eget justo fermentum fringilla. In hac habitasse platea dictumst.',
      loveIts: 0,
      created_at: new Date(),
    },
    {
      title: 'Second Post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non metus id ipsum rutrum laoreet. Nulla in sagittis mauris. Nam eget mollis urna. Aliquam vitae neque eget justo fermentum fringilla. In hac habitasse platea dictumst.',
      loveIts: 1,
      created_at: new Date(),
    },
    {
      title: 'Third Post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non metus id ipsum rutrum laoreet. Nulla in sagittis mauris. Nam eget mollis urna. Aliquam vitae neque eget justo fermentum fringilla. In hac habitasse platea dictumst.',
      loveIts: -1,
      created_at: new Date(),
    },
  ];
}
